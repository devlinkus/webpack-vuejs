import Vue from 'vue'
import Router from 'vue-router'

const RouterCfg = () => import(/* webpackChunkName: "group-router-Cfg" */ './views/RouterConfiguration')
const TestShow = () => import(/* webpackChunkName: "group-other" */ '@vue/TestShow')

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.NODE_ENV === 'development' ? process.env.ROUTER_BASE_DEV:process.env.ROUTER_BASE_PROD,
  routes: [
    {
      path: '/router_config',
      name: 'RouterConfiguration',
      component: RouterCfg
    },
    {
      path: '/show/:id',
      name: 'Show',
      component: TestShow
    }
  ]
})
