require('dotenv').config();
const Dotenv = require('dotenv-webpack')
const path = require('./utils.js')
const merge = require('webpack-merge')
const base = require('./webpack.base.js')
const ManifestPlugin = require('webpack-manifest-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')

module.exports = merge(base, {
  mode: 'production',
  output: {
    path: path.rootPath(process.env.OUTPUT_PATH),
    filename: '[name].[chunkhash].js',
    publicPath: process.env.OUTPUT_PUBLICPATH
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: ['babel-loader']
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader'
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [
                require('autoprefixer')({
                  browsers: ['last 2 version', 'ie > 8']
                })
              ]
            }
          },
          'sass-loader'
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].[hash].css'
    }),
    new ManifestPlugin(),
    new OptimizeCSSAssetsPlugin({
      cssProcessor: require('cssnano')
    })
  ]

})
