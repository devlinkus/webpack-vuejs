require('dotenv').config();
const Dotenv = require('dotenv-webpack')
const path = require('./utils.js')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

console.log(process.env.OUTPUT_PATH)
console.log('yolo')
module.exports = {
  entry: {
    app: path.rootPath('src/main.js')
  },
  output: {
    path: path.rootPath('public/dist/'),
    filename: '[name].js',
    publicPath: 'dist/'
  },
  resolve: {
    alias: {
      '@css': path.rootPath('src/assets/css'),
      '@img': path.rootPath('src/assets/img'),
      '@vue': path.rootPath('src/components')
    },
    extensions: ['.js', '.vue']
  },
  module: {
    rules: [
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i,
        loader: 'file-loader'
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              fallback: 'file-loader',
              name: '[name].[hash:7].[ext]'
            }
          },
          {
            loader: 'img-loader',
            options: {
              enabled: process.env.NODE_ENV === 'development'
            }
          }
        ]
      },
      {
        test: /\.vue$/,
        use: [
          {
            loader: 'vue-loader',
            options: {
              loaders: {
                'scss': [
                  'vue-style-loader',
                  'css-loader',
                  'sass-loader'
                ],
                'sass': [
                  'vue-style-loader',
                  'css-loader',
                  'sass-loader?indentedSyntax'
                ]
              }
            }
          }
        ]
      }

    ]
  },
  plugins: [
    new Dotenv({
      path: '.env',
    }),
    new CleanWebpackPlugin([path.rootPath(process.env.OUTPUT_PATH)], {
      root: path.rootPath(),
      verbose: true,
      dry: false,
      allowExternal: true
    }),
    new VueLoaderPlugin()
  ]
}
