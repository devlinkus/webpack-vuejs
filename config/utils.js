const path = require('path')

exports.rootPath = function (dir = '') {
  return path.resolve(__dirname, '..', dir)
}
